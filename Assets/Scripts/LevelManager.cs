﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour {
    public List<GameObject> foods = new List<GameObject>();
    public float spawnFrequency = 1.0f;

	// Use this for initialization
	void Start () {
        StartCoroutine(Generate());
    }
	
	// Update is called once per frame
	void Update () {

    }

    IEnumerator Generate()
    {
        yield return new WaitForSeconds(spawnFrequency);

        int index = Random.Range(0, foods.Count);
        Vector3 pos = new Vector3(0.0f, 5.5f, 0.0f);
        pos.x = Random.Range(-7.0f, 7.0f);
        GameObject food = Instantiate(foods[index], pos, Quaternion.identity) as GameObject;
        StartCoroutine(Generate());
    }
}
