﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {
    public float speed = 5.0f;
    private Vector3 velocity;
    private uint score;
    private uint lives;
    public Text scoreText;
    public Text livesText;

	// Use this for initialization
	void Start () {
        velocity = new Vector3(0.0f, 0.0f);
        score = 0;
        lives = 9;
        scoreText.text = "Score: " + score.ToString();
        livesText.text = "Lives: " + lives.ToString();
    }
	
	// Update is called once per frame
	void Update () {
		if (lives > 0)
        {
            if (Input.GetKey(KeyCode.RightArrow))
            {
                velocity.x = speed;
            }
            else if (Input.GetKey(KeyCode.LeftArrow))
            {
                velocity.x = -speed;
            }
            else
            {
                velocity.x = 0.0f;
            }
            transform.Translate(velocity * Time.deltaTime);

            if (transform.position.x > 7.5f)
            {
                Vector3 vec = transform.position;
                vec.x = 7.5f;
                transform.position = vec;
            }
            else if (transform.position.x < -7.5f)
            {
                Vector3 vec = transform.position;
                vec.x = -7.5f;
                transform.position = vec;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Food") && lives > 0)
        {
            score++;
            scoreText.text = "Score: " + score.ToString();
            Destroy(other.gameObject);
        }
    }

    public void LoseLife()
    {
        lives--;
        livesText.text = "Lives: " + lives.ToString();
        if (lives == 0)
        {
            StartCoroutine(ResetLevel());
        }
    }

    IEnumerator ResetLevel()
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
